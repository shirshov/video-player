function Player(options){
    var self = this;

    var icons = {
        playOutline: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">' +
            '<path fill="#FFFFFF" d="M12,20.14C7.59,20.14 4,16.55 4,12.14C4,7.73 7.59,4.14 12,4.14C16.41,4.14 20,7.73 20,12.14C20,16.55 16.41,20.14 12,20.14M12,2.14A10,10 0 0,0 2,12.14A10,10 0 0,0 12,22.14A10,10 0 0,0 22,12.14C22,6.61 17.5,2.14 12,2.14M10,16.64L16,12.14L10,7.64V16.64Z" />' +
            '</svg>',
        play: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">' +
            '<path fill="#FFFFFF" d="M8,5.14V19.14L19,12.14L8,5.14Z" />' +
            '</svg>',
        pause: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M14,19.14H18V5.14H14M6,19.14H10V5.14H6V19.14Z" />'+
            '</svg>',
        volumeOff: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M12,4L9.91,6.09L12,8.18M4.27,3L3,4.27L7.73,9H3V15H7L12,20V13.27L16.25,17.53C15.58,18.04 14.83,18.46 14,18.7V20.77C15.38,20.45 16.63,19.82 17.68,18.96L19.73,21L21,19.73L12,10.73M19,12C19,12.94 18.8,13.82 18.46,14.64L19.97,16.15C20.62,14.91 21,13.5 21,12C21,7.72 18,4.14 14,3.23V5.29C16.89,6.15 19,8.83 19,12M16.5,12C16.5,10.23 15.5,8.71 14,7.97V10.18L16.45,12.63C16.5,12.43 16.5,12.21 16.5,12Z" />' +
            '</svg>',
        volumeLow: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M7,9V15H11L16,20V4L11,9H7Z" />' +
            '</svg>',
        volumeMedium: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M5,9V15H9L14,20V4L9,9M18.5,12C18.5,10.23 17.5,8.71 16,7.97V16C17.5,15.29 18.5,13.76 18.5,12Z" />' +
            '</svg>',
        volumeHigh: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M14,3.23V5.29C16.89,6.15 19,8.83 19,12C19,15.17 16.89,17.84 14,18.7V20.77C18,19.86 21,16.28 21,12C21,7.72 18,4.14 14,3.23M16.5,12C16.5,10.23 15.5,8.71 14,7.97V16C15.5,15.29 16.5,13.76 16.5,12M3,9V15H7L12,20V4L7,9H3Z" />' +
            '</svg>',
        fullscreenOn: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M5,5H10V7H7V10H5V5M14,5H19V10H17V7H14V5M17,14H19V19H14V17H17V14M10,17V19H5V14H7V17H10Z" />'+
            '</svg>',
        fullscreenOff: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M14,14H19V16H16V19H14V14M5,14H10V19H8V16H5V14M8,5H10V10H5V8H8V5M19,8V10H14V5H16V8H19Z" />'+
            '</svg>'
    };

    self.container = options.element;
    self.element = self.container.querySelector("video");
    self.pause = self.container.querySelector(".pause");
    self.pause.innerHTML = icons["play"];
    self.pause.addEventListener('click', function(e){
        e.stopPropagation();
        if (self.element.paused){
            self.element.play();
        } else {
            self.element.pause();
        };
    });
    self.volume = self.container.querySelector(".volume");
    self.volume.innerHTML = icons["volumeHigh"];
    self.volume.addEventListener('click', function(e){
        e.stopPropagation();
        if (self.element.volume == 0){
            self.element.volume = 1;
        } else {
            self.element.volume = 0;
        };
    });

    self.autoplay = options.autoplay != undefined ? options.autoplay : true;
    self.cycle = options.cycle != undefined ? options.cycle : true;
    self.hoverSound = options.hoverSound != undefined ? options.hoverSound : true;

    self.getParams = function(str){
        str = decodeURI(str);
        if (str.length == 0){
            return {};
        };
        var q = str.split("?")[1];
        if (!q){

        };
        var params = {};
        var w = q.split("&");
        for (var i = 0; i < w.length; i++){
            var e = w[i].split("=");
            params[e[0]] = e[1];
        };
        if (params.ri){
            try{
                params.ri = JSON.parse(params.ri.toLowerCase());
            } catch (e){
                params.ri = true;
            };
        };
        return params;
    };
    self.getTrackString = function(params){
        var fields = ["seanceId", "rid", "iid"];
        var str = "";
        for (var i = 0; i < fields.length; i++){
            var field = fields[i];
            if (params[field]){
                if (str.length > 0){
                    str += "&";
                };
                str += (field + "=" + params[field]);
            };
        };
        return str;
    };
    self.params = self.getParams(location.search);
    self.trackStr = self.getTrackString(self.params);

    self.isIncVolume = false;
    self.isDecVolume = false;

    self.sendEvent = function(event){
        var paths = {
            play: "",
            pause: "SavePauseEvent",
            firstQuartile: "SaveFirstQuartileEvent",
            midpoint: "SaveMidpointEvent",
            thirdQuartile: "SaveThirdQuartileEvent",
            complete: "SaveCompleteEvent",
            unmute: "SaveUnmuteEvent",
            mute: "SaveMuteEvent",
            impression: "SaveImpression",
            view: "SaveView",
            start: "SaveStartEvent"
        };
        var path = paths[event];
        if (!path){
            return;
        };
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://dsa.targetix.net/DeliverySeance/' + path + '?' + self.trackStr);
        xhr.send();
    };

    self.systemEvents = {
        'play': {
            action: function(obj){
                if (!obj.was){
                    self.sendEvent('start');
                    self.sendEvent('play');
                    obj.was = true;
                };
                self.pause.innerHTML = icons["pause"];
            },
            was: false
        },
        'pause': {
            listen: function(obj){
                self.pause.addEventListener('click', function(e){
                    if (!obj.was){
                        self.sendEvent('pause');
                        obj.was = true;
                    };
                    e.stopPropagation();
                });
            },
            action: function(obj){
                self.pause.innerHTML = icons["play"];
            },
            was: false
        },
        'timeupdate': {
            action: function(obj){
                var q = self.element.currentTime / self.element.duration;
                if (!obj.firstQuartile && q > 0.25){
                    self.sendEvent("firstQuartile");
                    obj.firstQuartile = true;
                    return;
                };
                if (!obj.midpoint && q > 0.5){
                    self.sendEvent("midpoint");
                    obj.midpoint = true;
                    return;
                };
                if (!obj.thirdQuartile && q > 0.75){
                    self.sendEvent("thirdQuartile");
                    obj.thirdQuartile = true;
                    return;
                };
                if (!obj.complete && q >= 0.99){
                    self.sendEvent("complete");
                    self.sendEvent("view");
                    obj.complete = true;
                };
                if (self.cycle && q >= 0.99999){
                    self.element.currentTime = 0;
                    self.element.play();
                };
            },
            firstQuartile: false,
            midpoint: false,
            thirdQuartile: false,
            complete: false
        },
        'volumechange': {
            action: function(obj){
                var volume = self.element.volume;
                if (volume == 0){
                    self.volume.innerHTML = icons.volumeOff;
                } else {
                    self.volume.innerHTML = icons.volumeHigh;
                }
                if (volume > 0 && obj.prev == 0){
                    if (!obj.unmute){
                        self.sendEvent('unmute');
                        obj.unmute = true;
                    };
                    obj.prev = 1;
                } else if (volume == 0 && obj.prev == 1) {
                    if (!obj.mute){
                        self.sendEvent('mute');
                        obj.mute = true;
                    };
                    obj.prev = 0;
                };
            },
            prev: 0,
            mute: false,
            unmute: false
        }
    };

    for (var q in self.systemEvents){
        if (self.systemEvents[q].listen){
            self.systemEvents[q].listen(self.systemEvents[q]);
        };
        self.element.addEventListener(q, (function(obj){
            return function(){
                obj.action(obj);
            }
        })(self.systemEvents[q]));
    };

    if (self.autoplay){
        self.element.play();
    } else {

    };

    if (!self.hoverSound){
        self.element.volume = 1;
    } else {
        function hoverListener(e){
            self.element.volume = 1;
            self.container.removeEventListener('mouseover', hoverListener);
        };
        self.container.addEventListener('mouseover', hoverListener);
        self.element.volume = 0;
    };

    if (self.params.cu){
        document.body.addEventListener('click', function(e){
            self.element.pause();
            var win = window.open(self.params.cu, '_blank');
            //win.focus();
        });
    };

    if (self.params.ri){
        self.sendEvent("impression");
    };

};
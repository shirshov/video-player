﻿function Player(options) {
    var self = this;

    var icons = {
        playOutline: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">' +
            '<path fill="#FFFFFF" d="M12,20.14C7.59,20.14 4,16.55 4,12.14C4,7.73 7.59,4.14 12,4.14C16.41,4.14 20,7.73 20,12.14C20,16.55 16.41,20.14 12,20.14M12,2.14A10,10 0 0,0 2,12.14A10,10 0 0,0 12,22.14A10,10 0 0,0 22,12.14C22,6.61 17.5,2.14 12,2.14M10,16.64L16,12.14L10,7.64V16.64Z" />' +
            '</svg>',
        play: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">' +
                '<path fill="#FFFFFF" d="M8,5.14V19.14L19,12.14L8,5.14Z" />' +
            '</svg>',
        pause: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">'+
                '<path fill="#FFFFFF" d="M14,19.14H18V5.14H14M6,19.14H10V5.14H6V19.14Z" />'+
                '</svg>',
        volumeOff: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
                    '<path fill="#FFFFFF" d="M12,4L9.91,6.09L12,8.18M4.27,3L3,4.27L7.73,9H3V15H7L12,20V13.27L16.25,17.53C15.58,18.04 14.83,18.46 14,18.7V20.77C15.38,20.45 16.63,19.82 17.68,18.96L19.73,21L21,19.73L12,10.73M19,12C19,12.94 18.8,13.82 18.46,14.64L19.97,16.15C20.62,14.91 21,13.5 21,12C21,7.72 18,4.14 14,3.23V5.29C16.89,6.15 19,8.83 19,12M16.5,12C16.5,10.23 15.5,8.71 14,7.97V10.18L16.45,12.63C16.5,12.43 16.5,12.21 16.5,12Z" />' +
                '</svg>',
        volumeLow: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
                '<path fill="#FFFFFF" d="M7,9V15H11L16,20V4L11,9H7Z" />' +
            '</svg>',
        volumeMedium: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M5,9V15H9L14,20V4L9,9M18.5,12C18.5,10.23 17.5,8.71 16,7.97V16C17.5,15.29 18.5,13.76 18.5,12Z" />' +
        '</svg>',
        volumeHigh: '<svg style="width:20px;height:20px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M14,3.23V5.29C16.89,6.15 19,8.83 19,12C19,15.17 16.89,17.84 14,18.7V20.77C18,19.86 21,16.28 21,12C21,7.72 18,4.14 14,3.23M16.5,12C16.5,10.23 15.5,8.71 14,7.97V16C15.5,15.29 16.5,13.76 16.5,12M3,9V15H7L12,20V4L7,9H3Z" />' +
            '</svg>',
        fullscreenOn: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M5,5H10V7H7V10H5V5M14,5H19V10H17V7H14V5M17,14H19V19H14V17H17V14M10,17V19H5V14H7V17H10Z" />'+
            '</svg>',
        fullscreenOff: '<svg style="width:24px;height:24px" viewBox="0 0 24 24">'+
            '<path fill="#FFFFFF" d="M14,14H19V16H16V19H14V14M5,14H10V19H8V16H5V14M8,5H10V10H5V8H8V5M19,8V10H14V5H16V8H19Z" />'+
        '</svg>'
    };

    self.sendEvent = function (e) {
        //if (self.eventsWas.complete) {
        //    return;
        //};
        console.warn(e);
    };

    self.eventsWas = {
        start: false,
        firstQurtile: false,
        midpoint: false,
        thirdQuartile: false,
        complete: false,
        mute: true
    };

    self.clearEvents = function () {
        for (var k in self.eventsWas) {
            self.eventsWas[k] = false;
        };
    };

    function getVolumeSliderValue(e) {
        var t = 1 - (e.clientY - (self.volumeSliderContainer.getBoundingClientRect().top + 10)) / 100;
        if (t > 1) {
            return 1;
        } else if (t < 0.011) {
            return 0;
        } else {
            return t;
        }
    };

    function getTimeSliderValue(e) {
        var width = self.timeSliderContainer.offsetWidth - 20;
        var t = (e.clientX - (self.timeSliderContainer.getBoundingClientRect().left + 15)) / width * self.video.duration;
        if (t < 0) {
            return 0;
        } else if (t > self.video.duration) {
            return self.video.duration;
        } else {
            return t;
        };
    };

    //#region Events
    function onPlay() {
        self.element.classList.remove('paused');
        self.element.classList.remove('paused');
        if (self.eventsWas.complete) {
            self.clearEvents();
            self.sendEvent('rewind');
        };
        if (!self.eventsWas.start) {
            self.sendEvent('start');
            self.eventsWas.start = true;
        } else {
            self.sendEvent('resume');
        };
        self.playButton.innerHTML = icons.pause;
    };

    function onPause() {
        if (self.video.duration == self.video.currentTime) {
            self.sendEvent('complete');
            self.eventsWas.complete = true;
            self.element.classList.remove('paused');
            self.element.classList.add('complete');
        } else {
            self.sendEvent('pause');
            self.element.classList.add('paused');
        };
        self.playButton.innerHTML = icons.play;
    };

    function onFullScreen(e) {
        if (document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen) {
            onFullScreenOn();
        } else {
            onFullScreenOff();
        };
    };

    function onFullScreenOn() {
        self.sendEvent('fullscreen');
        self.element.classList.add('fullscreen');
        self.fullButton.innerHTML = icons.fullscreenOff;
    };

    function onFullScreenOff() {
        self.element.classList.remove('fullscreen');
        self.fullButton.innerHTML = icons.fullscreenOn;
    };

    function onTimeUpdate() {
        var duration = Math.floor(self.video.duration);
        var time = Math.floor(self.video.currentTime);
        var minutes = Math.floor(time / 60);
        var seconds = (time % 60);
        if (seconds.toString().length < 2) {
            seconds = '0' + seconds;
        };
        self.currentTime.innerHTML = minutes + ":" + seconds;

        var width = self.timeSliderContainer.offsetWidth - 20;

        self.timeSliderButton.style.left = 10 + (time / duration) * width + 'px';
        self.timeSliderBefore.style.width = (time / duration) * width + 5 + 'px';
        self.timeSliderAfter.style.width = (1 - (time / duration)) * width + 'px';

        if (time / duration > 0.25 && !self.eventsWas.firstQuartile) {
            self.sendEvent('firstQuartile');
            self.eventsWas.firstQuartile = true;
        };
        if (time / duration > 0.5 && !self.eventsWas.midpoint) {
            self.sendEvent('midpoint');
            self.eventsWas.midpoint = true;
        };
        if (time / duration > 0.75 && !self.eventsWas.thirdQuartile) {
            self.sendEvent('thirdQuartile');
            self.eventsWas.thirdQuartile = true;
        };
    };

    function onDurationChange() {
        var duration = Math.floor(self.video.duration);
        var minutes = Math.floor(duration / 60);
        var seconds = (duration % 60);
        if (seconds.toString().length < 2) {
            seconds = '0' + seconds;
        };
        self.duration.innerHTML = minutes + ":" + seconds;
    };

    function onVolumeChange() {
        var volume = self.video.volume;
        self.volumeSliderButton.style.top = 5 + (1 - volume) * 100 + 'px';
        self.volumeSliderAfter.style.height = (1 - volume) * 100 + 'px';
        self.volumeSliderBefore.style.height = volume * 100 + 'px';
        if (volume <= 0.01) {
            if (!self.eventsWas.mute) {
                self.sendEvent('mute');
                self.eventsWas.mute = true;
            };
            self.volumeButton.innerHTML = icons.volumeOff;
        } else {
            if (self.eventsWas.mute) {
                self.sendEvent('unmute');
                self.eventsWas.mute = false;
            };
            if (volume > 0 && volume < 0.33) {
                self.volumeButton.innerHTML = icons.volumeLow;
            } else if (volume >= 0.33 && volume < 0.67) {
                self.volumeButton.innerHTML = icons.volumeMedium;
            } else {
                self.volumeButton.innerHTML = icons.volumeHigh;
            };
        };
    };

    self.systemEvents = {
        'durationchange': onDurationChange,
        'play': onPlay,
        'pause': onPause,
        'timeupdate': onTimeUpdate,
        'volumechange': onVolumeChange
    };

    //#endregion

    self.fields = {
        playButton: {
            s: 'button.play-button',
            click: function () {
                self.togglePlay();
            }
        },
        fullButton: {
            s: 'button.fullscreen',
            click: function () {
                self.toggleFullScreen();
            }
        },
        duration: {
            s: '.duration'
        },
        currentTime: {
            s: '.current-time'
        },
        volumeButton: {
            s: '.volume-button',
            click: function () {
                self.toggleVolume();
            }
        },
        volumeSliderContainer: {
            s: '.volume-slider-container',
            click: function (e){
                self.video.volume = getVolumeSliderValue(e);
            },
            mousemove: function (e) {
                if (self.isChangeVolume) {
                    var v = getVolumeSliderValue(e);
                    self.video.volume = v;
                };
            },
            mouseleave: function (e) {
                self.isChangeVolume = false;
            },
            mouseup: function (e) {
                self.isChangeVolume = false;
            }
        },
        volumeSliderBefore: {
            s: '.volume-slider-before',
        },
        volumeSliderAfter: {
            s: '.volume-slider-after'
        },
        volumeSliderButton: {
            s: '.volume-slider-button',
            mousedown: function () {
                self.isChangeVolume = true;
            }
        },
        //time
        timeSliderContainer: {
            s: '.time-slider-container',
            click: function (e) {
                self.video.currentTime = getTimeSliderValue(e);
            },
            mousemove: function (e) {
                if (self.isChangeTime) {
                    var v = getTimeSliderValue(e);
                    self.video.currentTime = v;
                };
            },
            mouseleave: function (e) {
                self.isChangeTime = false;
            },
            mouseup: function (e) {
                self.isChangeTime = false;
            }
        },
        timeSliderBefore: {
            s: '.time-slider-before',
        },
        timeSliderAfter: {
            s: '.time-slider-after'
        },
        timeSliderButton: {
            s: '.time-slider-button',
            mousedown: function () {
                self.isChangeTime = true;
            }
        },
    };

    //#region Init

    self.element = options.element;

    self.element.addEventListener('click', function () {
        if (!self.isUserAction) {
            self.toggleVolume();
        };
        self.isUserAction = true;
    });

    self.video = self.element.querySelector('video');

    self.video.addEventListener('dblclick', function () {
        self.toggleFullScreen();
    });

    self.video.addEventListener('click', function () {
        self.togglePlay();
    });

    self.playCircle = self.element.querySelector('.play-circle');

    self.playCircle.addEventListener('click', function () {
        self.togglePlay();
    });

    self.controlsWrapper = self.element.querySelector('.video-controls-wrapper');

    self.controls = self.element.querySelector('.video-controls-container');

    for (var i in self.fields) {
        self[i] = self.controls.querySelector(self.fields[i].s);
        for (var j in self.fields[i]) {
            if (j == 's') {
                continue;
            };
            self[i].addEventListener(j, self.fields[i][j]);
        };
    };

    window.addEventListener('scroll', function () {
        checkScroll();
    });

    window.addEventListener('resize', function () {
        checkScroll();
        //if (window.innerHeight == screen.height) {
        //    onFullScreen();
        //}
    })

    document.addEventListener('fullscreenchange',onFullScreen);

    document.addEventListener('mozfullscreenchange', onFullScreen);

    document.addEventListener('webkitfullscreenchange', onFullScreen);

    document.addEventListener('MSFullscreenChange', onFullScreen);

    //#endregion

    for (var k in self.systemEvents) {
        self.video.addEventListener(k, self.systemEvents[k], false);
    };

    self.isInShadow = true;

    self.togglePlay = function () {
        if (self.video.paused) {
            self.video.play();
        } else {
            self.video.pause();
        }
    };

    self.toggleFullScreen = function () {
        if (document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen || document.msFullscreenElement) {
            if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.msExitFullscreen){
                document.msExitFullscreen();
            } else {
                document.exitFullscreen();
            };
        } else {
            if (self.element.requestFullscreen) {
                self.element.requestFullscreen();
            } else if (self.element.mozRequestFullScreen) {
                self.element.mozRequestFullScreen();
            } else if (self.element.webkitRequestFullscreen) {
                self.element.webkitRequestFullscreen();
            } else if (self.element.msRequestFullscreen) {
                self.element.msRequestFullscreen();
            };
        };
    };

    self.toggleVolume = function () {
        if (self.video.volume != 0) {
            self.prevVolume = self.video.volume;
            self.video.volume = 0;
        } else {
            self.video.volume = self.prevVolume;
        }
    };

    self.video.load();

    self.prevVolume = 0.4;

    self.video.volume = 0;

    function checkScroll() {
        var height = document.documentElement.clientHeight;
        var width = document.documentElement.clientWidth;

        var elRect = self.element.getBoundingClientRect();

        if ((elRect.top > 0) && (elRect.left > 0) && (elRect.bottom < height) && (elRect.right < width)) {
            if (self.isInShadow && !self.isUserAction) {
                self.video.play();
                self.isInShadow = false;
            }
        } else {
            if (self.eventsWas.start && !self.isUserAction) {
                self.video.pause();
            };
            self.isInShadow = true;
        };
    };

    checkScroll();

    self.element.addEventListener('mousemove', function () {
        var isFull = false;
        var cl = self.element.classList;
        for (var i = 0; i < cl.length; i++) {
            if (cl[i] == 'fullscreen') {
                isFull = true;
                break;
            }
        };

        if (isFull) {
            clearTimeout(self.timerControls);
            self.controlsWrapper.style.opacity = 1;
            self.timerControls = setTimeout(function () {
                self.controlsWrapper.style.opacity = '';
            }, 1000);
        };

    });

};